import java.util.Random;

public class Board {
	
	private Tile[][] grid;
	private final int ROWS;
	private final int COLUMNS;
	
	public Board() {
		
		ROWS = 5;
		COLUMNS = 5;
		
		this.grid = new Tile[ROWS][COLUMNS];
		
		intializeGrid();
		
	}
	
	// Method to initialize the grid
	private void intializeGrid() {
		
		// Used to make sure that there are 7 blanks
		final int MAX_HIDDEN_WALLS = (ROWS*COLUMNS) - 7;
		int hiddenWallCount = 0;
		
		for(int i = 0; i < this.grid.length; i++) {
			
			for(int j = 0; j < this.grid[i].length; j++) {
				
				// If the coin flip returns 1, then the tile is set to a hidden wall
				// if it returns 0, it stays a blank.
				
				int coinFlipResult = flipCoin();
				
				if(coinFlipResult == 1 && hiddenWallCount < MAX_HIDDEN_WALLS) {
					
					this.grid[i][j] = Tile.HIDDEN_WALL;
					hiddenWallCount++;
					
				}
				
				else {
					
					this.grid[i][j] = Tile.BLANK;
					
				}
				
			}
			
		}
		
	}
	
	// Method that simulates a coin flip
	private int flipCoin() {
		
		Random rand = new Random();
		
		final int NUM_FACES = 2;
		return rand.nextInt(NUM_FACES);
		
	}
	
	// Prints the grid
	public String toString() {
		
		String result = "";
		
		for(Tile[] row : this.grid) {
			
			result += "\n";
			
			for(Tile col : row) {
				
				result += "[" + col.getName() + "] ";
				
			}
			
			result += "\n";
			
		}
		
		return result;
		
	}
	
	/* Checks to see if the desired placement is valid,
	   returns -2 if row or column is invalid
	   returns -1 if there is already a castle or wall in that position
	   returns 1 if there is a hidden wall at the desired position and sets the tile there to a wall
	   return 0 if the placement is valid
	*/
	public int placeToken(int row, int col) {
		
		// Variables to check if row or column is placed out of bounds
		boolean validRowPlacement = row >= 0 && row < this.ROWS;
		boolean validColPlacement = col >= 0 && col < this.COLUMNS;
		
		if(!validRowPlacement || !validColPlacement) {
			
			return -2;
			
		}
		
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL) {
			
			return -1;
			
		}
		
		else if(this.grid[row][col] == Tile.HIDDEN_WALL) {
			
			this.grid[row][col] = Tile.WALL;
			return 1;
			
		}
		
		else {
			
			this.grid[row][col] = Tile.CASTLE;
			return 0;
			
		}
		
	}
	
}