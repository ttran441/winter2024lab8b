import java.util.Scanner;

public class BoardGameApp {
	
	public static void main(String[] args) {
		
		Board board = new Board();
		
		Scanner scanner = new Scanner(System.in);
		
		int numCastles = 7;
		int turns = 0;
		final int MAX_TURNS = 8;
		
		// Loop to run the game
		System.out.println("Welcome to the hidden wall game!");
		
		while(numCastles > 0 && turns < MAX_TURNS) {
			
			System.out.println("Turn " + (turns+1) + ":");
			System.out.println(board);
			System.out.println("Number of castles left: " + numCastles);
			
			System.out.println("");
			
			System.out.println("Enter desired row(1-5):");
			int row = Integer.parseInt(scanner.nextLine())-1; // Subtracts 1 to account for the 0 index
			
			System.out.println("");
			
			System.out.println("Enter desired column(1-5):");
			int column = Integer.parseInt(scanner.nextLine())-1; // ^ same reason
			
			int result = board.placeToken(row, column);
			
			while(result < 0) {
				
				System.out.println("Please enter proper values!");
				
				System.out.println("");
			
				System.out.println("Enter desired row(1-5):");
				row = Integer.parseInt(scanner.nextLine())-1;
				
				System.out.println("");
				
				System.out.println("Enter desired column(1-5):");
				column = Integer.parseInt(scanner.nextLine())-1;
				
				result = board.placeToken(row, column);
				
			}
			
			if(result == 1) {
				
				System.out.println("There was a hidden wall there!");
				
			}
			
			else {
				
				System.out.println("Castle successfully placed!");
				numCastles--;
				
			}
			
			System.out.println("");
			
			turns++;
			
		}
		
		System.out.println(board);
		
		// Check if won or lost
		if(numCastles == 0) {
			
			System.out.println("Congratulations, you've won!");
			
		}
		
		else {
			
			System.out.println("Oh no! You lost!");
			
		}
		
	}
	
}